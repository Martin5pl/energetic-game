
from sprites import *
from Timer import Timer


def print_fps():
    msg = ("FPS: " + str(globals.fps_calc.calculate_fps()))
    txt = font_small.render(msg, True, globals.BLUE)
    screen.blit(txt, (10, 10))


def print_speed(obj):
    msg = ("BLUE SPEED: " + str(round(obj.speed.length(), 3)))
    txt = font_small.render(msg, True, globals.BLUE)
    screen.blit(txt, (10, 10 + globals.FONT_SIZE_S))


def print_acceleration(obj):
    msg = ("BLUE ACCEL: " + str(round(100 * obj.acceleration.length(), 1)))
    txt = font_small.render(msg, True, globals.BLUE)
    screen.blit(txt, (10, 10 + 2 * globals.FONT_SIZE_S))


def print_target_energy(energy):
    msg = ("TARGET ENERGY: " + str(round(energy)))
    txt = font_large.render(msg, True, globals.BLUE)
    screen.blit(txt, (10, 10 + 2 * globals.FONT_SIZE_S + globals.FONT_SIZE_L))


def print_energy(energy):
    msg = ("TOTAL ENERGY: " + str(round(energy, 1)))
    txt = font_large.render(msg, True, globals.BLUE)
    screen.blit(txt, (10, 10 + 2 * globals.FONT_SIZE_S + 2 * globals.FONT_SIZE_L))


def print_timer(timer):
    if globals.game_finished:
        globals.delta += 1
        if globals.delta == globals.BLINKING:
            globals.delta = 0
            if globals.time_color == globals.BLACK:
                globals.time_color = globals.GREEN
            else:
                globals.time_color = globals.BLACK
    msg = ("TIME: " + str(timer.get_time(1)))
    txt = font_large.render(msg, True, globals.time_color)
    screen.blit(txt, (10, 10 + 2 * globals.FONT_SIZE_S + 3 * globals.FONT_SIZE_L))


def update_screen():
    screen.fill(globals.WHITE)
    print_fps()
    print_speed(ball_blue)
    print_acceleration(ball_blue)
    print_target_energy(globals.target_energy)
    print_energy(globals.total_energy)
    print_timer(timer)
    for sprite in sprites:
        sprite.update(mouse_clicked)
        sprite.render()


def update_logic():
    energy = 0
    for b in balls:
        energy += b.energy
    globals.total_energy = energy
    if globals.total_energy <= globals.target_energy:
        timer.finish_timer()
        if not globals.game_finished:
            timer_sound.play()
        globals.game_finished = True


def detect_collision(obj1, obj2):
    normal = obj2.position - obj1.position
    distance = normal.length()
    sizes = obj1.radius + obj2.radius
    if distance <= sizes:
        bounce_sound.play()

        normal = normal.normalize()
        tangent = Vector2(-normal.y, normal.x)

        dpTan1 = obj1.speed.x * tangent.x + obj1.speed.y * tangent.y
        dpTan2 = obj2.speed.x * tangent.x + obj2.speed.y * tangent.y

        dpNorm1 = obj1.speed.x * normal.x + obj1.speed.y * normal.y
        dpNorm2 = obj2.speed.x * normal.x + obj2.speed.y * normal.y

        momentum1 = (dpNorm1 * (obj1.mass - obj2.mass) + 2 * obj2.mass * dpNorm2) / (obj1.mass + obj2.mass)
        momentum2 = (dpNorm2 * (obj2.mass - obj1.mass) + 2 * obj1.mass * dpNorm1) / (obj1.mass + obj2.mass)

        obj1.speed.x = tangent.x * dpTan1 + normal.x * momentum1
        obj1.speed.y = tangent.y * dpTan1 + normal.y * momentum1

        obj2.speed.x = tangent.x * dpTan2 + normal.x * momentum2
        obj2.speed.y = tangent.y * dpTan2 + normal.y * momentum2


def detect_ball_collisions(balls):
    for i in range(len(balls) - 1):
        for j in range(i + 1, len(balls)):
            detect_collision(balls[i], balls[j])


pygame.init()
displayInfo = pygame.display.Info()
screen_x = displayInfo.current_w
screen_y = displayInfo.current_h


screen = pygame.display.set_mode((screen_x, screen_y), pygame.FULLSCREEN)
pygame.display.set_caption("Energetic game")
clock = pygame.time.Clock()
timer = Timer()
font_large = pygame.font.Font(None, globals.FONT_SIZE_L)
font_small = pygame.font.Font(None, globals.FONT_SIZE_S)
text_fps = font_small.render("FPS: ---", True, globals.BLUE)
text_speed = font_small.render("SPEED: ---", True, globals.BLACK)

sprites = []
balls = []
mouse_clicked = False
the_end = False

ball_blue = MegaBall(position=Vector2(screen_x / 2, screen_y / 2), speed=Vector2(0, 0), radius=45, color=globals.BLUE, screen=screen,
                     focused=True, density=1)
ball_red1 = MegaBall(position=Vector2(screen_x * 0.25, screen_y * 0.25), speed=Vector2(5, -1), radius=30, color=globals.RED, screen=screen)
ball_red2 = MegaBall(position=Vector2(screen_x * 0.75, screen_y * 0.25), speed=Vector2(-1, -5), radius=45, color=globals.RED, screen=screen)
ball_red3 = MegaBall(position=Vector2(screen_x * 0.75, screen_y * 0.75), speed=Vector2(-5, 1), radius=80, color=globals.RED, screen=screen)
line_to_cursor = LineToCursor(screen=screen, main_ball=ball_blue)
sprites.append(ball_blue)
sprites.append(ball_red1)
sprites.append(ball_red2)
sprites.append(ball_red3)
sprites.append(line_to_cursor)
balls.append(ball_blue)
balls.append(ball_red1)
balls.append(ball_red2)
balls.append(ball_red3)

timer.start_timer()

while not the_end:
    clock.tick(globals.FRAMES_PS)
    events = pygame.event.get()
    mouse_position = pygame.mouse.get_pos()
    for event in events:
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            the_end = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_clicked = True
        elif event.type == pygame.MOUSEBUTTONUP:
            mouse_clicked = False

    detect_ball_collisions(balls)

    update_logic()
    update_screen()

    pygame.display.flip()

pygame.quit()
