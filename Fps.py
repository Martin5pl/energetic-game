from time import time


class Fps:
    def __init__(self, counting_step):
        self._counting_step = counting_step
        self._previous = round(1000*time())

    def calculate_fps(self):
        current = round(1000 * time())
        result = round(self._counting_step * 1000 / (current - self._previous))
        self._previous = current
        return result
