### *Energetic Game*

**Target of the game:**  
There are 4 balls moving on the screen. Blue ball can be controlled with the mouse.
The target is to take away total kinetic energy from the balls and make it below "target energy" in the shortest time.
Good luck.

**Requirements:**  
Python 3.7  
pygame 1.9.4+ https://www.pygame.org/

**To install plugins:**  
```pip3 install -r requirements.txt```

**To start:**  
```python main.py```
