from time import time


class Timer:
    def __init__(self):
        self.start_time = 0
        self.end_time = 0
        self.finished = False

    def start_timer(self):
        self.start_time = round(1000 * time())
        self.finished = False

    def get_time(self, precision=2):
        end = round(1000 * time())
        if self.finished:
            seconds = (self.end_time - self.start_time) / 1000
        else:
            seconds = (end - self.start_time) / 1000
        return round(seconds, precision)

    def finish_timer(self):
        if not self.finished:
            self.end_time = round(1000 * time())
            self.finished = True
