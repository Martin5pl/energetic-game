from pygame import Vector2
from game_sounds import *
import globals


class MegaBall:
    def __init__(self, position, speed, radius, color, screen, focused=False, density=1):
        self.position = position
        self.speed = speed
        self.acceleration = Vector2(0, 0)
        self.radius = radius
        self.mass = radius ** 3 * density
        self.energy = self.speed.length() ** 2 * self.mass
        self.color = color
        self.screen = screen
        self.focused = focused
        self.factor = 0.0001
        self.screen_res = pygame.display.get_surface().get_size()
        self.font = pygame.font.Font(None, globals.FONT_SIZE_BALL)

    def set_energy(self):
        self.energy = self.speed.length() ** 2 * self.mass / 1000

    def update(self, action):
        if action and self.focused:
            mouse = Vector2(pygame.mouse.get_pos())
            ball = Vector2(self.position)
            vector = Vector2(mouse - ball)
            self.acceleration = self.factor * vector
        else:
            self.acceleration = Vector2(0)

        self.speed += self.acceleration
        self.set_energy()
        self.position += self.speed
        if self.position.x < self.radius and self.speed.x < 0:
            self.speed = self.speed.reflect(Vector2(1, 0))
            bounce_sound.play()

        if (self.position.x > self.screen_res[0] - self.radius) and self.speed.x > 0:
            self.speed = self.speed.reflect(Vector2(1, 0))
            bounce_sound.play()

        if self.position.y < self.radius and self.speed.y < 0:
            self.speed = self.speed.reflect(Vector2(0, 1))
            bounce_sound.play()

        if self.position.y > self.screen_res[1] - self.radius and self.speed.y > 0:
            self.speed = self.speed.reflect(Vector2(0, 1))
            bounce_sound.play()

    def render(self):
        pygame.draw.circle(self.screen, self.color, (round(self.position.x), round(self.position.y)), self.radius, 0)
        msg = (str(round(self.energy)))
        txt_size = self.font.size(msg)
        txt = self.font.render(msg, True, globals.BLACK)
        self.screen.blit(txt, (self.position.x - txt_size[0] / 2, self.position.y - txt_size[1] / 2))


class LineToCursor:
    def __init__(self, screen, main_ball):
        self.color = globals.BLACK
        self.screen = screen
        self.start_pos = [0, 0]
        self.end_pos = [100, 100]
        self.main_ball = main_ball

    def update(self, action):
        mouse = Vector2(pygame.mouse.get_pos())
        ball = Vector2(self.main_ball.position)
        vector = Vector2(mouse - ball)
        self.end_pos = mouse
        self.start_pos = self.main_ball.position + vector / 2
        if action:
            self.color = globals.RED
        else:
            self.color = globals.BLACK

    def render(self):
        pygame.draw.aaline(self.screen, self.color, self.start_pos, self.end_pos, True)


class Rectangle:
    def __init__(self, color, screen, position, size):
        self.color = color
        self.position = position
        self.size = size
        self.screen = screen

    def update(self, action):
        pass

    def render(self):
        location = [self.position.x, self.position.y, self.size.x, self.size.y]
        pygame.draw.rect(self.screen, self.color, location, 2)
