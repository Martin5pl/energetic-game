import pygame  # version 1.9.4

pygame.mixer.init(frequency=22050, buffer=64)
bounce_sound = pygame.mixer.Sound("tick.wav")
timer_sound = pygame.mixer.Sound("timer.wav")
