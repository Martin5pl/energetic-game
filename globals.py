from Fps import Fps


FRAMES_PS = 60
BLINKING = 15
BLACK = (0, 0, 0)
GRAY = (170, 170, 170)
WHITE = (255, 255, 255)
BLUE = (80, 80, 255)
GREEN = (80, 255, 80)
RED = (255, 100, 100)
FONT_SIZE_L = 28
FONT_SIZE_S = 20
FONT_SIZE_BALL = 24

delta = 0
time_color = BLACK
total_energy = 0
target_energy = 200
game_finished = False

fps_calc = Fps(1)
